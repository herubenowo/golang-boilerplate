package users

import (
	"errors"
	"golang-boilerplate/api/auth"
	"golang-boilerplate/api/libs/responses"
	"golang-boilerplate/api/libs/utility"
	"golang-boilerplate/api/models"
	"golang-boilerplate/config/validators"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

type ResponseToken struct {
	Token string `json:"token"`
}

func Login(db *gorm.DB, ctx echo.Context) error {
	log.Println(ctx.Request().URL)
	body, err := validators.Login(ctx)
	if err != nil {
		return ctx.JSON(http.StatusUnprocessableEntity, responses.ERROR(err))
	}
	token, err := SignIn(db, body.Email, body.Password)
	if err != nil {
		return ctx.JSON(http.StatusUnauthorized, responses.ERROR(err))
	}
	return ctx.JSON(http.StatusOK, responses.SUCCESS(ResponseToken{Token: token}))
}

func SignIn(db *gorm.DB, email string, password string) (string, error) {
	var err error
	user := models.User{}
	err = db.Debug().Model(models.User{}).Where("email = ?", email).Scan(&user).Error
	if user.ID == 0 {
		return "", errors.New("Email not found")
	} else if err != nil {
		return "", err
	}
	err = utility.VerifyPassword(user.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", errors.New("Password doesn't match")
	}
	return auth.CreateToken(user.ID)
}
