package users

import (
	"golang-boilerplate/api/libs/responses"
	"golang-boilerplate/api/middlewares"
	"golang-boilerplate/api/models"
	"golang-boilerplate/config/validators"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

func CreateUser(db *gorm.DB, ctx echo.Context) error {
	log.Println(ctx.Request().URL)
	err := middlewares.Auth(db, ctx, []string{"super_admin", "admin"})
	if err != nil {
		return ctx.JSON(http.StatusUnauthorized, responses.ERROR(err))
	}
	body, err := validators.CreateUser(ctx)
	if err != nil {
		return ctx.JSON(http.StatusUnprocessableEntity, responses.ERROR(err))
	}
	user := models.User{}
	err = user.CreateUser(body, db, ctx)
	if err != nil {
		log.Println(err)
		return ctx.JSON(http.StatusInternalServerError, responses.ERROR(err))
	}
	return ctx.JSON(http.StatusOK, responses.SUCCESS(true))
}

func GetAllUser(db *gorm.DB, ctx echo.Context) error {
	log.Println(ctx.Request().URL)
	err := middlewares.Auth(db, ctx, []string{"super_admin", "admin", "maker", "checker", "viewer"})
	if err != nil {
		return ctx.JSON(http.StatusUnauthorized, responses.ERROR(err))
	}
	body := new(models.Filter)
	if err := ctx.Bind(body); err != nil {
		return ctx.JSON(http.StatusInternalServerError, responses.ERROR(err))
	}
	user := models.User{}
	users, err := user.GetAll(body, db, ctx)
	if err != nil {
		log.Println(err)
		return ctx.JSON(http.StatusInternalServerError, responses.ERROR(err))
	}
	return ctx.JSON(http.StatusOK, responses.SUCCESS(users))
}

func GetUserByID(db *gorm.DB, ctx echo.Context) error {
	log.Println(ctx.Request().URL)
	err := middlewares.Auth(db, ctx, []string{"super_admin", "admin", "maker", "checker", "viewer"})
	if err != nil {
		return ctx.JSON(http.StatusUnauthorized, responses.ERROR(err))
	}
	user := models.User{}
	users, err := user.GetByID(db, ctx)
	if err != nil {
		log.Println(err)
		return ctx.JSON(http.StatusInternalServerError, responses.ERROR(err))
	}
	return ctx.JSON(http.StatusOK, responses.SUCCESS(users))
}

func UpdateUser(db *gorm.DB, ctx echo.Context) error {
	log.Println(ctx.Request().URL)
	err := middlewares.Auth(db, ctx, []string{"super_admin", "admin"})
	if err != nil {
		return ctx.JSON(http.StatusUnauthorized, responses.ERROR(err))
	}
	body, err := validators.UpdateUser(db, ctx)
	if err != nil {
		return ctx.JSON(http.StatusUnprocessableEntity, responses.ERROR(err))
	}
	user := models.User{}
	err = user.UpdateUser(body, db, ctx)
	if err != nil {
		log.Println(err)
		return ctx.JSON(http.StatusInternalServerError, responses.ERROR(err))
	}
	return ctx.JSON(http.StatusOK, responses.SUCCESS(true))
}

func DeleteUser(db *gorm.DB, ctx echo.Context) error {
	log.Println(ctx.Request().URL)
	err := middlewares.Auth(db, ctx, []string{"super_admin", "admin"})
	if err != nil {
		return ctx.JSON(http.StatusUnauthorized, responses.ERROR(err))
	}
	user := models.User{}
	err = user.DeleteUser(db, ctx)
	if err != nil {
		log.Println(err)
		return ctx.JSON(http.StatusInternalServerError, responses.ERROR(err))
	}
	return ctx.JSON(http.StatusOK, responses.SUCCESS(nil))
}
