package responses

type ResponseSuccess struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ResponseWithPagination struct {
	Message  string      `json:"message"`
	Data     interface{} `json:"data"`
	Page     int         `json:"page"`
	PageSize int         `json:"page_size"`
}

type ResponseError struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}

func ERROR(err error) interface{} {
	resp := ResponseError{Message: "Error occured", Error: err.Error()}
	return resp
}

func SUCCESS(data interface{}) interface{} {
	resp := ResponseSuccess{Message: "Success retrieve data", Data: data}
	return resp
}

func SUCCESS_WITH_PAGINATION(data interface{}, page int, page_size int) interface{} {
	resp := ResponseWithPagination{Message: "Success retrieve data", Data: data, Page: page, PageSize: page_size}
	return resp
}
