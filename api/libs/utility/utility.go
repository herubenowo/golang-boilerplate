package utility

import (
	"errors"
	"fmt"
	"strings"

	"golang.org/x/crypto/bcrypt"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

type Pagination struct {
	Page     int `json:"page" form:"page" query:"page"`
	PageSize int `json:"page_size" form:"page_size" query:"page_size"`
}

func HashPassword(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func ParseError(err error, ctx echo.Context) error {
	if castedobject, ok := err.(validator.ValidationErrors); ok {
		for _, err := range castedobject {
			switch err.Tag() {
			case "required":
				return errors.New(fmt.Sprintf("%s is required", err.Field()))
			case "email":
				return errors.New(fmt.Sprintf("%s is not a valid email", err.Field()))
			}
		}
	}
	return nil
}

func ArrayContainsString(str string, arr []string) bool {
	for _, s := range arr {
		if strings.ToLower(str) == strings.ToLower(s) {
			return true
		}
	}
	return false
}
