package models

import (
	"github.com/jinzhu/gorm"
)

type Role struct {
	gorm.Model
	Code string `gorm:"size:255; not null;unique" json:"code" form:"code" query:"code" validate:"required"`
	Name string `gorm:"size:255;not null;" json:"name" form:"name" query:"name" validate:"required"`
}
