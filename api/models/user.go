package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"

	"golang-boilerplate/api/libs/utility"
)

type User struct {
	gorm.Model
	Name     string `gorm:"size:255;not null;" json:"name" form:"name" query:"name" validate:"required"`
	Email    string `gorm:"size:255;not null;unique" json:"email" form:"email" query:"email" validate:"required,email"`
	Password string `gorm:"size:255;not null;" json:"password" form:"password" query:"password" validate:"required"`
}

type BodyUser struct {
	Name     string `gorm:"size:255;not null;" json:"name" form:"name" query:"name" validate:"required"`
	Email    string `gorm:"size:255;not null;unique" json:"email" form:"email" query:"email" validate:"required,email"`
	Password string `gorm:"size:255;not null;" json:"password" form:"password" query:"password" validate:"required"`
	RoleID   uint   `gorm:"not null;" json:"role_id" form:"role_id" query:"role_id" validate:"required"`
}

type EntityUser struct {
	ID        uint      `json:"id" form:"id" query:"id"`
	Name      string    `json:"name" form:"name" query:"name"`
	Email     string    `json:"email" form:"email" query:"email"`
	Roles     Role      `json:"roles"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type CheckID struct {
	ID uint `json:"id" form:"id" query:"id" validate:"required"`
}

type Filter struct {
	Q        string `json:"q" form:"q" query:"q"`
	Page     int    `json:"page" form:"page" query:"page"`
	PageSize int    `json:"page_size" form:"page_size" query:"page_size"`
}

func (user *User) CreateUser(body *BodyUser, db *gorm.DB, ctx echo.Context) error {
	var err error
	check := CheckID{}
	db.Debug().Model(&User{}).Where("email = ?", body.Email).Scan(&check)
	if check.ID != 0 {
		return errors.New("Email already exist")
	}

	hashedPassword, err := utility.HashPassword(body.Password)
	if err != nil {
		return err
	}

	user.Name = body.Name
	user.Email = body.Email
	user.Password = string(hashedPassword)
	check = CheckID{}
	db.Model(&Role{}).Where("id = ?", body.RoleID).Scan(&check)
	if check.ID == 0 {
		return errors.New("Role not found")
	}
	err = db.Debug().Create(&user).Take(&user).Error
	if err != nil {
		return err
	}
	userRole := UserRole{}
	err = userRole.CreateUserRole(db, user.ID, body.RoleID)
	if err != nil {
		return err
	}
	return nil
}

func (user *User) GetAll(body *Filter, db *gorm.DB, ctx echo.Context) (*[]EntityUser, error) {
	var err error
	if err = ctx.Bind(body); err != nil {
		return &[]EntityUser{}, err
	}
	result := []EntityUser{}
	if body.Q != "" {
		filterQ := "%" + body.Q + "%"
		db = db.Debug().Model(&User{}).Where("LOWER(name) LIKE ? OR LOWER(email) LIKE ?", filterQ, filterQ)
	} else {
		db = db.Debug().Model(&User{})
	}
	if body.Page >= 1 && body.PageSize >= 1 {
		offset := (body.Page - 1) * body.PageSize
		err = db.Offset(offset).Limit(body.PageSize).Scan(&result).Error
	} else {
		err = db.Limit(100).Scan(&result).Error
	}

	if err != nil {
		return &[]EntityUser{}, err
	}
	for i, r := range result {
		userRole := UserRole{}
		db.Model(&UserRole{}).Where("user_id = ?", r.ID).Take(&userRole)
		db.Model(&Role{}).Where("id = ?", &userRole.RoleID).Take(&userRole.Role)
		result[i].Roles = userRole.Role
	}
	return &result, nil
}

func (user *User) GetByID(db *gorm.DB, ctx echo.Context) (*EntityUser, error) {
	result := EntityUser{}
	err := db.Debug().Model(&User{}).Where("id = ?", ctx.Param("id")).Scan(&result).Error
	if result.ID == 0 {
		return &EntityUser{}, errors.New("Data not found")
	} else if err != nil {
		return &EntityUser{}, err
	}
	userRole := UserRole{}
	db.Model(&UserRole{}).Where("user_id = ?", result.ID).Take(&userRole)
	db.Model(&Role{}).Where("id = ?", &userRole.RoleID).Take(&userRole.Role)
	result.Roles = userRole.Role
	return &result, nil
}

func (user *User) UpdateUser(body *BodyUser, db *gorm.DB, ctx echo.Context) error {
	var err error
	hashedPassword, err := utility.HashPassword(body.Password)
	if err != nil {
		return err
	}
	check := CheckID{}
	db.Model(&Role{}).Where("id = ?", body.RoleID).Scan(&check)
	if check.ID == 0 {
		return errors.New("Role not found")
	}
	err = db.Debug().Model(&user).Where("id = ?", ctx.Param("id")).UpdateColumns(map[string]interface{}{
		"password":   string(hashedPassword),
		"name":       body.Name,
		"email":      body.Email,
		"updated_at": time.Now(),
	}).Take(&user).Error

	if err != nil {
		return err
	}

	userRole := UserRole{}
	err = userRole.UpdateUserRole(db, user.ID, body.RoleID)
	if err != nil {
		return err
	}
	return nil
}

func (user *User) DeleteUser(db *gorm.DB, ctx echo.Context) error {
	var err error
	if ctx.Param("id") == "" {
		return errors.New("ID is required")
	}
	id := ctx.Param("id")
	err = db.Debug().Delete(&User{}, id).Error
	if err != nil {
		return err
	}
	userRole := UserRole{}
	err = userRole.DeleteUserRole(db, id)
	if err != nil {
		return err
	}
	return nil
}
