package models

import (
	"github.com/jinzhu/gorm"
)

type UserRole struct {
	gorm.Model
	UserID uint `gorm:"not null;unique;"`
	User   User `gorm:"foreignKey:UserID"`
	RoleID uint `gorm:"not null;"`
	Role   Role `gorm:"foreignKey:RoleID"`
}

func (ur *UserRole) CreateUserRole(db *gorm.DB, user_id uint, role_id uint) error {
	err := db.Model(&User{}).Where("id = ?", user_id).Take(&ur.User).Error
	if err != nil {
		return err
	}
	err = db.Model(&Role{}).Where("id = ?", role_id).Take(&ur.Role).Error
	if err != nil {
		return err
	}
	err = db.Model(&UserRole{}).Create(&ur).Error
	if err != nil {
		return err
	}
	return nil
}

func (ur *UserRole) UpdateUserRole(db *gorm.DB, user_id uint, role_id uint) error {
	role := Role{}
	err := db.Model(&Role{}).Where("id = ?", role_id).Take(&role).Error
	if err != nil {
		return err
	}
	err = db.Model(&UserRole{}).Where("user_id = ?", user_id).UpdateColumns(map[string]interface{}{
		"role_id": role.ID,
	}).Error
	if err != nil {
		return err
	}
	return nil
}

func (ur *UserRole) DeleteUserRole(db *gorm.DB, user_id string) error {
	err := db.Model(&UserRole{}).Where("user_id = ?", user_id).Take(&ur).Error
	if err != nil {
		return err
	}
	err = db.Delete(&ur).Error
	if err != nil {
		return err
	}
	return nil
}
