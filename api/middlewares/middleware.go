package middlewares

import (
	"errors"
	"golang-boilerplate/api/auth"
	"golang-boilerplate/api/libs/responses"
	"golang-boilerplate/api/libs/utility"
	"golang-boilerplate/api/models"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

func SetAuthentication(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		err := auth.TokenValid(ctx)
		if err != nil {
			return ctx.JSON(http.StatusUnauthorized, responses.ERROR(err))
		}
		return next(ctx)
	}
}

func Auth(db *gorm.DB, ctx echo.Context, roles []string) error {
	id, err := auth.ExtractTokenID(ctx)
	if err != nil {
		log.Println(err)
		return err
	} else if id == 0 {
		err = errors.New("User not found")
		log.Println(err)
		return err
	}
	userRole := models.UserRole{}
	err = db.Debug().Model(&models.UserRole{}).Where("user_id = ?", id).Take(&userRole).Error
	if err != nil {
		log.Println(err)
		return err
	}
	err = db.Debug().Model(&models.Role{}).Where("id = ?", userRole.RoleID).Take(&userRole.Role).Error
	if err != nil {
		log.Println(err)
		return err
	}
	check := utility.ArrayContainsString(userRole.Role.Code, roles)
	if !check {
		err = errors.New("Role not authorized")
		log.Println(err)
		return err
	}
	return nil
}
