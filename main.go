package main

import (
	"golang-boilerplate/config"
	"golang-boilerplate/config/dbinstance"
)

func main() {
	db := dbinstance.InitConnection()
	config.InitRoutes(":3000", db)
}
