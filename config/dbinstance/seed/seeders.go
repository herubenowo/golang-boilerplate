package seed

import (
	"golang-boilerplate/api/libs/utility"
	"golang-boilerplate/api/models"

	"github.com/jinzhu/gorm"
)

var roles = []models.Role{
	models.Role{
		Code: "super_admin",
		Name: "Super Admin",
	},
	models.Role{
		Code: "admin",
		Name: "Admin",
	},
	models.Role{
		Code: "maker",
		Name: "Maker",
	},
	models.Role{
		Code: "checker",
		Name: "Checker",
	},
	models.Role{
		Code: "viewer",
		Name: "Viewer",
	},
}

var account = models.User{
	Name:     "Super Admin",
	Email:    "super_admin@mail.com",
	Password: "12345678",
}

func LoadSeed(db *gorm.DB) {
	for i, _ := range roles {
		result := models.CheckID{}
		db.Debug().Model(&models.Role{}).Where("LOWER(code) = ?", &roles[i].Code).Scan(&result)
		if result.ID == 0 {
			db.Debug().Model(&models.Role{}).Create(&roles[i])
		}
	}

	result := models.CheckID{}
	db.Debug().Model(&models.User{}).Where("email = ? OR Name = ?", &account.Email, &account.Name).Scan(&result)
	if result.ID == 0 {
		hashedPassword, _ := utility.HashPassword(account.Password)
		account.Password = string(hashedPassword)
		db.Debug().Model(&models.User{}).Create(&account)
	}
	resultAdmin := models.User{}
	resultRole := models.Role{}
	db.Debug().Model(&models.User{}).Where("name = ?", "Super Admin").Scan(&resultAdmin)
	db.Debug().Model(&models.Role{}).Where("code = ?", "super_admin").Scan(&resultRole)
	db.Debug().Model(&models.UserRole{}).AddForeignKey("user_id", "users(id)", "cascade", "cascade").AddForeignKey("role_id", "roles(id)", "cascade", "cascade")
	result = models.CheckID{}
	db.Debug().Model(&models.UserRole{}).Where("user_id = ?", resultAdmin.ID).Scan(&result)
	if result.ID == 0 {
		db.Debug().Model(&models.UserRole{}).Create(&models.UserRole{User: resultAdmin, Role: resultRole})
	}
}
