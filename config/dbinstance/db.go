package dbinstance

import (
	"fmt"
	"golang-boilerplate/api/models"
	"golang-boilerplate/config/dbinstance/seed"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

func InitConnection() *gorm.DB {
	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, not comming through %v", err)
	} else {
		log.Println("We are getting the env values")
	}
	DBURL := fmt.Sprintf("host=%s port=%s dbname=%s sslmode=disable password=%s", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"), os.Getenv("DB_PASSWORD"))
	db, err := gorm.Open(os.Getenv("DB_DRIVER"), DBURL)
	if err != nil {
		log.Printf("Cannot connect to %s database\n", os.Getenv("DB_DRIVER"))
		log.Fatal("This is the error:", err)
	} else {
		log.Printf("Connected to the %s database\n", os.Getenv("DB_DRIVER"))
	}

	db.Debug().AutoMigrate(&models.User{}, &models.Role{}, &models.UserRole{})
	seed.LoadSeed(db)
	return db
}
