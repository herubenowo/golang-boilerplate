package config

import (
	"golang-boilerplate/api/controllers/users"
	"golang-boilerplate/api/middlewares"
	"log"

	"github.com/go-playground/validator/v10"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		return err
	}
	return nil
}

func InitRoutes(addr string, db *gorm.DB) {
	r := echo.New()
	r.Validator = &CustomValidator{validator: validator.New()}

	r.POST("/login", func(ctx echo.Context) error {
		resp := users.Login(db, ctx)
		return resp
	})

	groupUsers := r.Group("/users")
	groupUsers.Use(middlewares.SetAuthentication)
	groupUsers.POST("/", func(ctx echo.Context) error {
		resp := users.CreateUser(db, ctx)
		return resp
	})
	groupUsers.GET("/", func(ctx echo.Context) error {
		resp := users.GetAllUser(db, ctx)
		return resp
	})
	groupUsers.GET("/:id", func(ctx echo.Context) error {
		resp := users.GetUserByID(db, ctx)
		return resp
	})
	groupUsers.PUT("/:id", func(ctx echo.Context) error {
		resp := users.UpdateUser(db, ctx)
		return resp
	})
	groupUsers.DELETE("/:id", func(ctx echo.Context) error {
		resp := users.DeleteUser(db, ctx)
		return resp
	})

	log.Printf("Server running at localhost%s\n", addr)
	r.Logger.Fatal(r.Start(addr))
}
