package validators

import (
	"errors"
	"golang-boilerplate/api/libs/utility"
	"golang-boilerplate/api/models"
	"log"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

type loginData struct {
	Email    string `json:"email" form:"email" query:"email" validate:"required,email"`
	Password string `json:"password" form:"password" query:"password" validate:"required"`
}

func Login(ctx echo.Context) (*loginData, error) {
	body := new(loginData)
	if err := ctx.Bind(body); err != nil {
		log.Println(err)
		return body, err
	}
	if err := ctx.Validate(body); err != nil {
		err = utility.ParseError(err, ctx)
		log.Println(err)
		return body, err
	}
	return body, nil
}

func CreateUser(ctx echo.Context) (*models.BodyUser, error) {
	body := new(models.BodyUser)
	if err := ctx.Bind(body); err != nil {
		return body, err
	}
	if err := ctx.Validate(body); err != nil {
		err = utility.ParseError(err, ctx)
		log.Println(err)
		return body, err
	}
	return body, nil
}

func UpdateUser(db *gorm.DB, ctx echo.Context) (*models.BodyUser, error) {
	body := new(models.BodyUser)
	if ctx.Param("id") == "" {
		return body, errors.New("ID is required")
	}
	if err := ctx.Bind(body); err != nil {
		return body, err
	}
	if err := ctx.Validate(body); err != nil {
		err = utility.ParseError(err, ctx)
		log.Println(err)
		return body, err
	}
	check := models.CheckID{}
	db.Debug().Model(&models.User{}).Where("id <> ? AND email = ?", ctx.Param("id"), body.Email).Scan(&check)
	if check.ID != 0 {
		return body, errors.New("Email already exist")
	}
	return body, nil
}
